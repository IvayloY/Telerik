ID1. **Parking calculator**

**Title** - Wrong price calculation for one and a half day

**Summary** - When try to calculate the price for one and a half day, the calculator produce a wrong result

**Steps to reproduce**
1. Go to landing page - http://adam.goucher.ca/parkcalc
1. Set the the calculator to calculate the price for one and a half day
2. Click  “Calculate” button

**Expected result** - The price to be $42 

**Actual result** - The price is $50

**Severity** - Major

**Environment** - Live

**Browsers** - Chrome, Mozilla,Safari,Edge, Opera,IE11

**Additional Information** - Demo - https://www.youtube.com/watch?v=dAXSXelNy6I






ID. **Travel agency**

**Title** - “How to book” page is missing on the Russian version of the site

**Summary** - When clicking on “How to book” link in the footer, the user is sent to the correct URL address - https://www.phptravels.net/How-to-Book  but the content of the page is missing

**Steps to reproduce**
1. Go to Home page https://www.phptravels.net
2. Scroll down to the bottom of the page 
3. Click “How to book” link

**Expected result** - The correct page is opened and the user see the correct information 

**Actual result** - The correct page is opened but the content is missing

**Severity** - High

**Environment** - Staging

**Browsers** - Chrome, Mozilla,Safari,Edge, Opera,IE11

**Additional Information** - Demo - https://www.youtube.com/watch?v=sskNLS1BYvY




ID3. **Travel agency 2**


**Title** - German language of the site does not work

**Summary** - When select ”German” from the language drop down menu, the site content is not translated 

**Steps to reproduce**
1. Go to Home page  https://www.phptravels.net/en
2. Select ”German” from the language drop down menu

**Expected result** - The site loads its German version

**Actual result** - The site stays in English

**Severity**  - High

**Environment** - Staging

**Browser** - Chrome, Mozilla,Safari,Edge, Opera,IE11

**Additional Information** - Demo - https://www.youtube.com/watch?v=d3gwOP8PwKc




ID4. **Parking calculator2**


**Title** - Price calculation is wrong for more than 24 hours

**Summary** - When more than 24 hours are selected, the calculator calculate wrong price

**Steps to reproduce**
1. Go to the calculator lading page - http://adam.goucher.ca/parkcalc/
2. Select Short-Term Parking from the drop down menu for Choose a Lot section
3. Enter 12:00 for entry date hour
4. Choose AM for entry date
5. Enter 8/10/2019 for an entry date day.
6. Enter 12:00  for leaving date
7. Choose AM for leaving date
8. Enter 12:00  for leaving date
9. Click on the ”Calculate” button

**Expected result** - If the cost for 1 day is $28, the 2-day cost should be $56.

**Actual result** - The calculated cost for 2 days is $54.

**Severity** - High

**Environment** - Live

**Browsers** - Chrome, Mozilla,Safari,Edge, Opera,IE11

**Additional Information** - Demo - https://www.youtube.com/watch?v=FgGPHWzd1dw




