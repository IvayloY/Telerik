ID1.

**Summary**  - Format functionality does not work

**Steps to reproduce**
1. Mark a word or piece of text with the mouse
2. From Format drop down menu select  any of the options

**Expected result** - The marked text should be formated as per option selected from the drop down menu

**Actual resul** - The marked text does not chanage at all

**Severity** - Medium

**Prerequisites** - Some text should be written in the RTE

**Environment** - Staging

**Browsers** -Chrome, Mozilla,Safari,Edge, Opera




ID2.

**Summary** - Justify button does not work

**Steps to reproduce**
1. Mark a word or piece of text with the mouse
2. Click "Justify" button

**Expected result** - The marked text should be align so the left and right sides of the text block both have a straight edge

**Actual resul** - The marked text does not chanage its position if it is left aligned. - If the marked text is not left aligned by default, it is moved to left.

**Severity** - High

**Prerequisites** - Some text should be written in the RTE

**Environment** - Staging

**Browsers** -Chrome, Mozilla,Safari,Edge, Opera




ID3.

**Summary** - Insert link button does not work

**Steps to reproduce**
1. Mark a word or piece of text with the mouse
2. Click "Insert link" button
3. Insert link pop up appears
4. Fill in all feilds (Web adress and Title. "Text" field is filled in by default )
5. Click "Insert"

**Expected result** - The marked text become a link to the address that we addeed in the "Web address" input of the pop up

**Actual result** -  The marked text does not become a link it is just underlined 

**Severity** -  High

**Prerequisites** - Some text should be written in the RTE

**Environment** - Staging

**Browsers** -  Chrome, Mozilla,Safari,Edge, Opera,IE 11












