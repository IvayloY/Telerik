 **1. Find as many bugs as you can on the Bug Example image in the resources.**
 
 * "Log-in" text in the blue button is not centered 
 * "Sign in" should be replaced with "Log in". 
 * "Login via facebook" - facebook starts with a small letter 
 * There is a useless image before "Login via Twitter" text 
 * Password field - it will be good if a useful placeholder is given instead of just asterisks. 
 * "Email or login" placeholder should be betetr replaced with someting like "Email or username" 
 * What happens if I do not remember my passowrd? There is no way to change it. 
 * It will be good if there is a "Remember me" checkbox. 
 * Sign up and Sign in buttons are not aligned correctly 
 * The spaces between the facebook logo and the text "Login via facebook" and the logo before "Login via Twitter" text are different. 


    
    
**3. Prioritize the test cases for using a microwave. Use priority levels from 1 to 3, where 1 means highest priority.**
    
   * Power goes off while the microwave is working. - **Priority 1**
   * The display shows accurate time. - Priority 2
   * The microwave starts when the start button is pressed. - **Priority 1**
   * The microwave stops when the stop button is pressed. - **Priority 1**
   * Selection of program changes the mode of the microwave. - **Priority 2**
   * The light inside turns on when the door is opened. - **Priority 3**
