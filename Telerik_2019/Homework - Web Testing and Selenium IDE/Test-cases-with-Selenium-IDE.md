ID1.

**Title** 
    - Create new topic


**Description**
     - As a logged in user I want to create a new topic in the forum


**Preconditions**
- I am logged in in the forum
- I have rights to create a new topic



**Steps to reproduce**

1. Go to the landing page - https://schoolforum.telerikacademy.com
2. Click "+New Topic" button
3. A new sections appers at the bottom of the page
4. Write a valid title in the Title field.
5. Write a valid comment in the comment text area.
6. Click "+ Create Topic" button


**Input data**
- Input for the title - *This is test with Selenium IDE*
- Input for the comment - *test selenium IDE*


**Expected result**
 - The section for creating new topic is closed and I am redirected to a page where my new topic is created.
  The topic has the title I have written and the the comment contains the text I put in the commnet field.

**Priority**
 - Prio 1
 
 
 
ID2.
 
 
 **Title**
    -  Replay to a comment


**Description**
    - As a logged in user I want to replay to a comment in the forum


**Preconditions**
- I am logged in in the forum
- I have rights to post comments


**Steps to reproduce**

1. Go to the landing page - https://schoolforum.telerikacademy.com
2. Click on the title of existing topic
3. You are redirected to the page of the topic and all comments are listed. The latest comment is at the bottom.
4. Click replay button below the latest comment
5. A new text area field appears at the bottom
6. Write a valid text in this field
7. Click replay button below the textarea



**Input data**
- Input data for the comment text area field - *This is a test comment*


**Expected result**
- The section for creating new comment is closed and I see my comment at the bottom of the tread
 

**Priority**
 - Prio 1
 
 
 
 
ID3.
 
 
**Title**
    - Test  "Search" functionality


**Description**
    - As a logged user I want to put a text in the search field and if there is a match, the search field to show me the approproate topics and comments.


**Preconditions**
- I am logged in in the forum


**Steps to reproduce**

1. Go to the landing page - https://schoolforum.telerikacademy.com
2. Click on the magnifying glass icon
3. An input field should appear
4. Write a text that you know that the topics or comments contain
5. A list with valid suggestions should appear below the input field
7. Click on one of them
8. You should be redirected to the appropriare page as per your selection


**Input data**
- Input data for the input field - *IDE*


**Expected result**
- Afetr clicking one of the suggested results, I should be redirected to the appropriate page
 

**Priority**
 - Prio 2
 
 
 
 
 